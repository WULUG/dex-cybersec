#include <stddef.h>
#include <stdio.h>
#include <signal.h>

char* my_strcpy(char *dest, char *src) {
	size_t i = 0;
	for (; src[i]; i++)
		dest[i] = src[i];
	dest[i] = 0x00;
	return dest;
}

void print_name(char* input_name) {
	char name[0x100];
	my_strcpy(name, input_name);
	printf("Hello: %s\n", name);
}

int main(int argc, char *argv[]) {
    raise(SIGSTOP);
    char tmp[0x200];
    FILE* f = fopen("input.txt", "rb");
    fread(tmp, 0x200, 1, f);
    fclose(f);
	print_name(tmp);
	return 0;
}

all:
	gcc -m32 test.c -o test -O0 -fno-pie -fno-pic -fno-stack-protector -fno-builtin -z execstack -D_FORTIFY_SOURCE=0 -g

clean:
	rm -f test
